public class Manusia {
        private String nama;
        private int umur;
        private int uang;
        private float kebahagiaan;

        //constructor
        public Manusia(String nama, int umur, int uang){
            this.nama = nama;
            this.umur = umur;
            this.uang = uang;
            this.kebahagiaan = 50;
        }
        public Manusia(String nama, int umur){
            this.nama = nama;
            this.umur = umur;
            this.uang = 50000;
            this.kebahagiaan = 50;
        }

        //setters getters
        public void setNama(String nama){
            this.nama = nama;
        }
        public void setUmur(int umur){
            this.umur = umur;
        }
        public void setUang(int uang){
            this.uang = uang;
        }
        public void setKebahagiaan(float kebahagiaan){
            this.kebahagiaan = kebahagiaan;
        }
        public int getUang(){
            return uang;
        }
        public int getUmur(){
            return umur;
        }
        public String getNama(){
            return nama;
        }
        public float getKebahagiaan(){
            return kebahagiaan;
        }
   

    public void beriUang(Manusia penerima){
        int totalAscii=0;
        for (int i=0; i<(penerima.nama).length(); i++){
            char karakter = (penerima.nama).charAt(i);
            totalAscii += (int)karakter;
        }
        int jumlahUang = totalAscii*100;

        //kebahagiaan baru
        if (this.uang >= jumlahUang){
            System.out.printf("%s memberi uang sebanyak %s kepada %s, mereka berdua senang :D\n", this.nama, jumlahUang, penerima.nama);
            float penambahanKebahagiaan = ((float)jumlahUang)/6000;
            this.kebahagiaan += penambahanKebahagiaan;
            penerima.kebahagiaan += penambahanKebahagiaan;
            this.kebahagiaan = Math.min(100, this.kebahagiaan);
            penerima.kebahagiaan = Math.min(100, penerima.kebahagiaan);
            this.uang -= jumlahUang;
            penerima.setUang(penerima.getUang()+jumlahUang);
        }
        else{
            System.out.printf("%s ingin memberi uang kepada %s, namun tidak memiliki cukup uang :'(\n", this.nama, penerima.nama);
        }
    }

    public void beriUang(Manusia penerima, int jumlahUang) {
        //kebahagiaan baru
        if (this.uang >= jumlahUang){
            System.out.printf("%s memberi uang sebanyak %s kepada %s, mereka berdua senang :D\n", this.nama, jumlahUang, penerima.nama);
            float penambahanKebahagiaan = ((float)jumlahUang)/6000;
            this.kebahagiaan += penambahanKebahagiaan;
            penerima.kebahagiaan += penambahanKebahagiaan;
            this.kebahagiaan = Math.min(100, this.kebahagiaan);
            penerima.kebahagiaan = Math.min(100, penerima.kebahagiaan);
            this.uang -= jumlahUang;
            penerima.setUang(penerima.getUang()+jumlahUang);
        }
        else{
            System.out.printf("%s ingin memberi uang kepada %s, namun tidak memiliki cukup uang :'(\n", this.nama, penerima.nama);
        }
    }

    public void bekerja(int durasi, int bebanKerja){
        if (this.umur<18) {
            System.out.println(this.nama+ " belum boleh kerja karena masih dibawah umur D:");
        }
        else{
            int BebanKerjaTotal = durasi * bebanKerja;
            if (BebanKerjaTotal <= this.kebahagiaan){
                this.kebahagiaan = this.kebahagiaan - BebanKerjaTotal;
                float pendapatan = BebanKerjaTotal*10000;
                System.out.println(this.nama+"bekerja full time, total pendapatan: "+pendapatan);
                this.uang += pendapatan;
            }
            else if (BebanKerjaTotal > kebahagiaan){
                int DurasiBaru = (int)kebahagiaan/bebanKerja;
                BebanKerjaTotal = DurasiBaru * bebanKerja;
                int pendapatan = BebanKerjaTotal*10000;

                this.kebahagiaan = Math.max((this.kebahagiaan-BebanKerjaTotal), 0);
                System.out.println(this.nama+" tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan: "+pendapatan);
                this.uang += pendapatan;
            }
        }
    }

    public void rekreasi(String namaTempat) {
        float Biaya = namaTempat.length() * 10000;
        if (this.uang >= Biaya) {
            this.kebahagiaan += namaTempat.length();
            this.kebahagiaan = Math.min(100, this.kebahagiaan);
            this.uang -= Biaya;
            System.out.println(this.nama+"berekreasi di "+namaTempat+", "+this.nama+" senang :)");
        } else{
        System.out.printf("%s tidak mempunyai cukup uang untuk berekreasi di %s :(\n", this.nama, namaTempat);
        }
    }

    public void sakit(String namaPenyakit) {
        this.kebahagiaan -= namaPenyakit.length();
        if (this.kebahagiaan<=0) {
            this.kebahagiaan = 0;
        }
        System.out.println(this.nama + " terkena penyakit " + namaPenyakit + " :0");
    }

    public String toString(){
        return("Nama\t\t: "+this.nama+"\nUmur\t\t: "+this.umur+"\nUang\t\t: "+this.uang+"\nKebahagiaan\t: "+this.kebahagiaan);
    }
}
