public class Penggajian {
    int gajiPerbulan;

    //method untuk menggaji semua karyawan
    public static void gajian(){
        System.out.println("Semua karyawan telah diberikan gaji");
        //buat counter semua karyawan +1
        for (Karyawan i: Korporasi.getListKaryawan()){
            i.setCounter(i.getCounter()+1);
            //kalau sudah 6 kali gajian, naikin gajinya
            if (i.getCounter() % 6 == 0){
                naikGaji(i.getNama(),i.getGaji());
                i.setGaji(i.getGaji()*110/100);
            }
            if ((i instanceof Staff) && (i.getGaji()>=Staff.getBatasGaji())){
                Karyawan tmp = new Manager(i.getNama(),i.getGaji());
                Korporasi.getListKaryawan().set(Korporasi.getListKaryawan().indexOf(i),tmp);
                System.out.printf("Selamat, %s telah dipromosikan menjadi MANAGER\n", tmp.getNama());
            }
        }
    }

    //method yang mengurus kenaikan gaji
    public static void naikGaji(String nama, int gajiAwal){
        int gajiNow = gajiAwal*110/100;
        System.out.println(nama+ " mengalami kenaikan gaji sebesar 10% dari "+ gajiAwal+ " menjadi "+ gajiNow);
    }


}
