import java.util.ArrayList;

public class Manager extends Karyawan{
    protected ArrayList<Karyawan> bawahanManager = new ArrayList<Karyawan>();

    public ArrayList<Karyawan> getBawahanManager() {
        return bawahanManager;
    }

    public void setBawahanManager(ArrayList<Karyawan> bawahanManager) {
        this.bawahanManager = bawahanManager;
    }

    public Manager(String nama, int gajiAwal){
        super(nama, gajiAwal);
        this.tipe = "MANAGER";
    }

    public void tambahBawahan(Karyawan atasan, Karyawan bawahan) {
        System.out.println("masuk man");
        if (this.bawahanManager.size() < 10) {
            this.bawahanManager.add(bawahan);
            System.out.printf("Karyawan %s telah menjadi bawahan %s\n", bawahan.getNama(), atasan.getNama());
        } else {
            System.out.printf("Tidak dapat menambahkan %s\n",bawahan.getNama());
        }
    }

}