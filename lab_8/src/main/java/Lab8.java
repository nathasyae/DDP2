import java.util.ArrayList;
import java.util.Scanner;

class Lab8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        //ngeset batas gaji staff
        int batasGaji = input.nextInt();
        Staff.setBatasGaji(batasGaji);
        input.nextLine();

        //inputan lain
        while (true) {
            String inputan = input.nextLine();
            //kalo gajian
            if (inputan.equals("GAJIAN")) {
                Penggajian.gajian();
            } //kalo lainnya
            else {
                //urusan split2 & bikin arrayinputan
                String[] arrayInputan = inputan.split(" ");
                if (arrayInputan.length == 4) {
                    arrayInputan[3] = String.valueOf(Integer.parseInt(arrayInputan[3]));
                }

                //kondisi2
                //kalo status
                if (arrayInputan[0].equals("STATUS")){
                    status(arrayInputan[1]);
                }
                //kalo tambah karyawan
                else if (arrayInputan[0].equals("TAMBAH_KARYAWAN")){
                    tambahKaryawan(arrayInputan[1],arrayInputan[2], Integer.parseInt(arrayInputan[3]));
                }
                else {
                    tambahBawahan(arrayInputan[1],arrayInputan[2]);
                }
            }

        }
    }

    //method untuk mereturn objek yg dicari
    public static Karyawan findObject(String nama){
        for (Karyawan i: Korporasi.listKaryawan){
            if (i.getNama().equals(nama)){
                return i;
            }
        } return null;
    }

    //method untuk menambah karyawan ke korporasi
    public static void tambahKaryawan(String tipe, String nama, int gajiAwal) {
        if (findObject(nama) == null && Korporasi.listKaryawan.size() <= Korporasi.MAX_KARYAWAN) {
            if (tipe.equals("MANAGER")) {
                Karyawan newKaryawan = new Manager(nama, gajiAwal);
                Korporasi.listKaryawan.add(newKaryawan);
            } else if (tipe.equals("STAFF")) {
                if (gajiAwal>=Staff.getBatasGaji()){
                    Karyawan newKaryawan = new Manager(nama, gajiAwal);
                    Korporasi.listKaryawan.add(newKaryawan);
                } else {
                    Karyawan newKaryawan = new Staff(nama, gajiAwal);
                    Korporasi.listKaryawan.add(newKaryawan);
                }
            } else if (tipe.equals("INTERN")) {
                Karyawan newKaryawan = new Intern(nama, gajiAwal);
                Korporasi.listKaryawan.add(newKaryawan);
            }

            System.out.printf("%s mulai bekerja sebagai %s di PT. Tampan\n", nama, findObject(nama).getTipe());
        } else {
            System.out.printf("Karyawan dengan nama %s telah terdaftar\n", nama);
        }
    }

    //memberikan informasi gaji perbulan karyawan
    public static void status(String nama){
        if (!(findObject(nama)==null)) {
            System.out.printf("%s %d\n", nama, findObject(nama).getGaji());
        } else {
            System.out.println("Karyawan tidak ditemukan");
        }
    }

    //method untuk menambahkan bawahan
    public static void tambahBawahan(String atasan, String bawahan){
        Karyawan atasanOb = findObject(atasan);
        Karyawan bawahanOb = findObject(bawahan);
        if (!(atasanOb==null)&& !(bawahanOb==null)){
            System.out.println("x");
            if (atasanOb instanceof Manager){
                System.out.println("x");
                if (!(bawahanOb instanceof Manager)){
                    System.out.println("x");
                ((Manager) atasanOb).tambahBawahan(atasanOb,bawahanOb);
                } else {
                    System.out.println("Anda tidak layak memiliki bawahan");
                }
            }
            else if (atasanOb instanceof Staff) {
                if (bawahanOb instanceof Intern) {
                    ((Staff) atasanOb).tambahBawahan(atasanOb, bawahanOb);
                } else {
                    System.out.println("Tidak bisa menjadikan dia bawahan");
                }
            }
            else {
                System.out.println("Anda tidak layak memiliki bawahan");
            }
        } else{
            System.out.println("Nama tidak berhasil ditemukan");
        }
    }

}