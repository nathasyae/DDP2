public class Karyawan extends Korporasi{
	protected String nama;
	protected String tipe;
	protected int gaji;
	protected int counter;

	protected int batasBawahan = 10;

	public Karyawan(String nama, int gajiAwal){
		this.nama = nama;
		this.gaji = gajiAwal;
		this.counter = 0;
	}

	public Karyawan(String nama, String tipe, int gajiAwal){
		this.nama = nama;
		this.tipe = tipe;
		this.gaji = gajiAwal;
		this.counter = 0;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	public int getGaji() {
		return gaji;
	}

	public void setGaji(int gaji) {
		this.gaji = gaji;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}


}