import java.util.ArrayList;

public abstract class Korporasi {
    protected static ArrayList<Karyawan> listKaryawan = new ArrayList<Karyawan>();

    public static int MAX_KARYAWAN = 10000;

    public static ArrayList<Karyawan> getListKaryawan() {
        return listKaryawan;
    }

    public static void setListKaryawan(ArrayList<Karyawan> listKaryawan) {
        Korporasi.listKaryawan = listKaryawan;
    }

    public static int getMaxKaryawan() {
        return MAX_KARYAWAN;
    }

    public static void setMaxKaryawan(int maxKaryawan) {
        MAX_KARYAWAN = maxKaryawan;
    }

}
