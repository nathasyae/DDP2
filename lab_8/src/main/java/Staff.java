import java.util.ArrayList;

public class Staff extends Karyawan{
	private static int BATAS_GAJI;
	private ArrayList<Karyawan> bawahanStaff = new ArrayList<Karyawan>();

	public static int getBatasGaji() {
		return BATAS_GAJI;
	}

	public static void setBatasGaji(int batasGaji) {
		BATAS_GAJI = batasGaji;
	}

	public Staff(String nama, int gajiAwal){
		super(nama, gajiAwal);
		this.tipe = "STAFF";
	}

	public void tambahBawahan(Karyawan atasan, Karyawan bawahan){
		System.out.println("msk ke staff");
		if (this.bawahanStaff.size() < 10) {
			this.bawahanStaff.add(bawahan);
			System.out.printf("Karyawan %s telah menjadi bawahan %s\n", bawahan.getNama(), atasan.getNama());
		} else {
			System.out.printf("Tidak dapat menambahkan %s\n",bawahan.getNama());
		}
	}

}