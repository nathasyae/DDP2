package character;

//  write Magician Class here
public class Magician extends Human{

    public Magician(String name, int hp){
        super(name, hp);
        this.tipe = "Magician";
    }

    public String burn(Player pemain){
        if (this.getTipe().equals("monster") || this.getTipe().equals("human")) {
        } else {
            if (pemain.getTipe().equals("Magician")) {
                pemain.setHp(pemain.getHp() - 20);
            } else {
                pemain.setHp(pemain.getHp() - 10);
            }
            if (pemain.getHp()<=0 || pemain.isBurned()==true) {
                pemain.setBurned(true);
                pemain.setAlive(false);
                return "Nyawa " + pemain.getName() + " " + pemain.getHp() + "\n dan matang";
            }
            return "Nyawa " + pemain.getName() + " " + pemain.getHp();
        } return "";
    }

}