package character;

//  write Monster Class here
public class Monster extends Player{
    private String roar;

    public String getRoar() {
        return roar;
    }

    public void setRoar(String roar) {
        this.roar = roar;
    }

    public Monster(String name, int hp){
        super(name, 2*hp);
        this.tipe = "Monster";
        this.roar = "AAAAAAaaaAAAAAaaaAAAAAA";
    }

    public Monster(String name, String tipe, int hp, String roar){
        super(name, tipe, 2*hp);
        this.tipe = "Monster";
        this.roar = roar;
    }

    public String roar() {
        if (this.tipe.equals("Monster")) {
            if (this.roar == null) {
                return "AAAAAAaaaAAAAAaaaAAAAAA";
            }
            return this.roar;
        } return "Tidak bisa berteriak";
    }

}