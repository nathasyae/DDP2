package character;

import java.util.ArrayList;

//  write Player Class here
public class Player{
    protected String name;
    protected int hp;
    protected String tipe;
    protected ArrayList<Player> diet = new ArrayList<>();
    protected boolean isAlive;
    protected boolean isBurned;


    public boolean isBurned() {
        return isBurned;
    }

    public void setBurned(boolean burned) {
        isBurned = burned;
    }

    public String isAlive() {
        if (isAlive==true){
            return "Masih hidup";
        } else {
            return "Sudah meninggal dunia dengan damai";
        }
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    //setter getter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        if (hp<=0){
            this.hp = 0;
            this.setAlive(false);
        } else {
            this.hp = hp;
        }
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public ArrayList<Player> getDiet() {
        return diet;
    }

    public void setDiet(ArrayList<Player> diet) {
        this.diet = diet;
    }

    //constructor

    public Player(String name, int hp, String tipe) {
        this.name = name;
        this.hp = hp;
        this.tipe = tipe;
        if (this.hp==0){
            this.isAlive = false;
        } else {
            this.isAlive = true;
        }
        this.isBurned = false;
        this.diet = null;
    }

    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
        if (this.hp==0){
            this.isAlive = false;
        } else {
            this.isAlive = true;
        }
        this.isBurned = false;
        this.diet = null;
    }

    public Player(String name, String tipe, int hp) {
        this.name = name;
        this.hp = hp;
        if (this.hp==0){
            this.isAlive = false;
        } else {
            this.isAlive = true;
        }
        this.isBurned = false;
        this.diet = null;
    }

    public boolean canEat(Player pemain){
        //kalo human
        //if (this.tipe)
        //kalo magician
        //kalo monster
        //cek ada ga playernya
        if (this!=null){
            Player me = this;
            //kalo ada player, cek idup ga
            if (me.isAlive().equals("Masih hidup")){
                //kalo idup, cek enemynya exist ga
                if (pemain!=null){
                    Player vs = pemain;
                    //kalo exist enemynya, cek tipe playernya
                    //kalo Human/magician
                    if (me.getTipe().equals("Human") || (me.getTipe().equals("Magician"))){
                        //cek tipe enemy
                        //kl enemynya monster
                        if (vs.getTipe().equals("Monster")){
                            //cek kebakar ga
                            if (vs.isBurned()==true){
                               return true;
                            } //kl ga kebakar
                        }//kl enemynya bukan monster
                        else {
                            return false;
                        }
                    } //kl playernya monster
                    else {
                        //cek enemynya udah mati blm
                        if (pemain.isAlive().equals("Sudah meninggal dunia dengan damai")) {
                            return true;
                        } //kalo blm mati
                        else {
                            return false;
                        }
                    }

                }
            }
        } return false;
    }



    public String attack(Player pemain){
        if (this==null){
            return "Tidak ada "+pemain.getName();
        } else {
            if (this.isAlive){
                this.setHp(this.getHp()-10);
                if (this.getHp()<=0){
                    this.setAlive(false);
                }
                return "Nyawa "+pemain.getName()+" "+this.getHp();
            } else {
                return this.name+ " tidak bisa menyerang "+pemain.getName();
            }
        }
    }
    


}