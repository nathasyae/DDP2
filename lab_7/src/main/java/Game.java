import character.*;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    //ArrayList<Player> diett = new ArrayList<Player>();
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player i: player){
            if (i.getName().equals(name)){
                return i;
            }
        } return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        if (find(chara)!=null){
            return "Sudah ada karakter bernama "+chara;
        } else {
            if (tipe.equals("Monster")){
                player.add(new Monster(chara,hp));
            } else if (tipe.equals("Magician")){
                player.add(new Magician(chara,hp));
            } else if (tipe.equals("Human")){
                player.add(new Human(chara,hp));
            }
            //System.out.println("cek "+player.get(player.size()-1).getName()+ " "+player.get(player.size()-1).getTipe());
            return chara+" ditambah ke game";
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        if (tipe.equals("Monster")){
            player.add(new Monster(chara,tipe,hp,roar));
            return chara+" ditambahkan ke game";
        } else {
            add(chara,tipe,hp);
        }
        return "";
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @param meName
     * @param enemyName
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara) {
        if (find(chara) != null) {
            player.remove(find(chara));
            return chara+" dihapus dari game";
        } else {
                return "Tidak ada " + chara;
        }
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        String foodies = "";
        String info="";
        if (find(chara) != null) {
                if (find(chara).getDiet() != null) {
                    foodies+="Memakan ";
                    for (int j = 0; j < find(chara).getDiet().size(); j++) {
                        foodies += find(chara).getDiet().get(j).getTipe() + " " + find(chara).getDiet().get(j).getName();
                    }
                } else {
                    foodies = "Belum memakan siapa siapa";
                }
                info += find(chara).getTipe() +" "+ chara + "\n" +
                        "HP: " + find(chara).getHp() + "\n" +
                        find(chara).isAlive() + "\n" +
                        foodies+"\n";
                }
            return info;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String info="";
        if (!(player.isEmpty())) {
            for (Player i : player) {
                info+=status(i.getName());
            }
        } return info;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
    String foodies = "";
    if (find(chara)!=null){
        if (find(chara).getDiet()!=null){
            //System.out.println("ga kosong");
            for (Player i: find(chara).getDiet()){
                //System.out.println("masuk for");
                foodies+= i.getTipe()+" "+i.getName();
                //System.out.println("cek "+foodies);
                }
            } return "Belum ada yang termakan";
        } return foodies;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String listnya="";
        if (player.size()>0) {
            for (Player i : player) {
                listnya += diet(i.getName()) + "\n";
            }
            return listnya;
        } return "";
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        if (find(meName)==null){
            return "Tidak ada "+meName;
        }
        else if (find(enemyName)==null){
            return "Tidak ada "+enemyName;
        } else {
            if (find(meName).isAlive().equals("Masih hidup")) {
                find(enemyName).setHp(find(enemyName).getHp() - 10);
                if (find(enemyName).getHp() <= 0) {
                    find(enemyName).setAlive(false);
                }
                return "Nyawa " + enemyName + " " + find(enemyName).getHp();
            } return meName+" tidak bisa menyerang "+enemyName;
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName) {
        if (find(meName).getTipe().equals("monster") || find(meName).getTipe().equals("human")) {
        } else {
            if (find(meName).isAlive().equals("Masih hidup")) {
                if (find(enemyName).getTipe().equals("Magician")) {
                    find(enemyName).setHp(find(enemyName).getHp() - 20);
                } else {
                    find(enemyName).setHp(find(enemyName).getHp() - 10);
                }
                if (find(enemyName).getHp() <= 0 || find(enemyName).isBurned() == true) {
                    find(enemyName).setBurned(true);
                    find(enemyName).setAlive(false);
                    return "Nyawa " + find(enemyName).getName() + " " + find(enemyName).getHp() + "\n dan matang";
                }
                return "Nyawa " + find(enemyName).getName() + " " + find(enemyName).getHp();
            }
        }return meName+" tidak dapat membakar "+enemyName;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param meName nama dari character yang sedang dimainkan
     * @param enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName) {
        ArrayList<Player> temp = new ArrayList<Player>();
        //cek ada ga playernya
        if (find(meName)!=null){
            Player me = find(meName);
            //kalo ada player, cek idup ga
            if (me.isAlive().equals("Masih hidup")){
                //kalo idup, cek enemynya exist ga
                if (find(enemyName)!=null){
                    Player vs = find(enemyName);
                    //kalo exist enemynya, cek tipe playernya
                    //kalo Human/magician
                    if (me.getTipe().equals("Human") || (me.getTipe().equals("Magician"))){
                        //cek tipe enemy
                        //kl enemynya monster
                        if (vs.getTipe().equals("Monster")){
                            //cek kebakar ga
                            if (vs.isBurned()==true){
                                me.setHp(me.getHp()+15);
                                //temp = find(meName).getDiet();
                                if (find(meName).getDiet()==null){
                                    temp.add(0,find(enemyName));
                                }
                                else {
                                    temp.add(find(enemyName));
                                }
                                find(meName).setDiet(temp);
                                player.remove(find(enemyName));
                                return meName+" memakan "+enemyName+"\nNyawa "+meName+" kini "+find(meName).getHp();
                            } //kl ga kebakar
                        }//kl enemynya bukan monster
                        else {
                            return meName + " tidak bisa memakan " + enemyName;
                        }
                    } //kl playernya monster
                    else {
                        //cek enemynya udah mati blm
                        if (find(enemyName).isAlive().equals("Sudah meninggal dunia dengan damai")) {
                            me.setHp(me.getHp()+15);
                            //temp = find(meName).getDiet();
                            if (find(meName).getDiet()==null){
                                temp.add(0,find(enemyName));
                            }
                            else {
                                temp.add(find(enemyName));
                            }
                            find(meName).setDiet(temp);
                            player.remove(find(enemyName));
                            return meName + " memakan " + enemyName + "\nNyawa " + meName + " kini " + find(meName).getHp();
                        } //kalo blm mati
                        else {
                            return meName + " tidak bisa memakan "+enemyName;
                        }
                    }

                } //kl ga exist enemynya
                return "Tidak ada "+enemyName;

            } //kalo mati
            return meName + " tidak bisa memakan "+enemyName;

        } //kalo gada playernya
        return "Tidak ada "+meName;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        if (find(meName)!=null) {
            if (find(meName) instanceof  Monster) {
                return ((Monster)find(meName)).getRoar();
            } else {
                return meName + " tidak bisa berteriak";
            }
        } return "Tidak ada " + meName;
    }

}