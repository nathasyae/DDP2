//POLOSAN SOAL WAJIB LAB 3
/**nathasya e
1706979404
ddp d
lab3
*/


import java.util.Scanner;

public class RabbitHouse {
    
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        String tipe = input.next();
        String nama = input.next();

        System.out.println(rabbit(nama));

    }

    public static int rabbit(String x){
        //base case
        if (x.length()==1) {
            return (1);
        }
        //recursion
        else{
            return( (x.length()*rabbit(x.substring(1))) + 1) ;
        }

    }

}
