package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Nathasya Eliora K.
 */
public class XoxoView {

    private JFrame frame;
    private JFrame warning;
    private JPanel panelInput;
    private JPanel panelButton;
    private JPanel panelLog;


    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;


    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        //base
        frame = new JFrame("Secret Message");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(500,500));

        //panel
        panelInput = new JPanel();
        panelButton = new JPanel();
        panelLog = new JPanel();

        //textfield&area
        messageField = new JTextField();
        keyField = new JTextField();
        seedField = new JTextField();
        logField = new JTextArea();

        //button
        encryptButton = new JButton("encrypt");
        decryptButton = new JButton("decrypt");

        //Label
        JLabel labelMsg = new JLabel("Message: ");
        JLabel labelKey = new JLabel("Key: ");
        JLabel labelSeed = new JLabel("Seed: ");
        JLabel labelLog = new JLabel("The Result");

        //add
        panelInput.setLayout(new GridLayout(3,2));
        panelInput.add(labelKey);
        panelInput.add(keyField);
        panelInput.add(labelSeed);
        panelInput.add(seedField);
        panelInput.add(labelMsg);
        panelInput.add(messageField);

        panelButton.add(encryptButton);
        panelButton.add(decryptButton);

        panelLog.add(logField);

        frame.add(BorderLayout.NORTH,panelInput);
        frame.add(BorderLayout.SOUTH,panelButton);
        frame.add(BorderLayout.CENTER,panelLog);

        //finishing
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    /**
     * Untuk memunculkan dialog warning
     */
    public void setWarning(String s) {
        JOptionPane.showMessageDialog(this.warning, s,"Error",JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Untuk membersihkan Log
     */
    public void clearLog(){
        this.logField.setText(null);
    }

}