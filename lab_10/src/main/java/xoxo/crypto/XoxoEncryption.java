package xoxo.crypto;

import com.sun.glass.ui.Size;
import org.w3c.dom.ranges.Range;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

/**
 * This class is used to create an encryption instance
 * that can be used to encrypt a plain text message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoEncryption {

    /**
     * A Kiss Key object that is required to encrypt the message.
     */
    private KissKey kissKey;

    /**
     * Maksimal message adalah 10Kbit = 1250 byte
     */
    private final int MAX_BYTE = 1250;

    /**
     * Class constructor with the given Kiss Key
     * string to build the Kiss Key object.
     *
     * @throws KeyTooLongException if the length of the
     *                             kissKeyString exceeded 28 characters.
     */
    public XoxoEncryption(String kissKeyString) throws KeyTooLongException {
        if (kissKeyString.length()>28){
            throw new KeyTooLongException("The kiss key is too long (max 28)");
        }
        for (int i = 0; i<kissKeyString.length(); i++) {
            if ((kissKeyString.charAt(i) < 64 && kissKeyString.charAt(i) > 90) ||
                (kissKeyString.charAt(i) < 97 && kissKeyString.charAt(i) > 122)) {
                    throw new InvalidCharacterException("Kiss Key invalid, hanya boleh mengandung huruf A-Z, a-z, dan @");
                }
            }

        this.kissKey = new KissKey(kissKeyString);
    }

    /**
     * Encrypts a message in order to make it unreadable.
     *
     * @param message The message that wants to be encrypted.
     * @return A XoxoMessage object that contains the encrypted message
     * and a Hug Key object that can be used to decrypt the message.
     */
    public XoxoMessage encrypt(String message) {
        String encryptedMessage = this.encryptMessage(message);
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey));
    }

    /**
     * Encrypts a message in order to make it unreadable.
     *
     * @param message The message that wants to be encrypted.
     * @param seed    A number to generate different Hug Key.
     * @return A XoxoMessage object that contains the encrypted message
     * and a Hug Key object that can be used to decrypt the message.
     * @throws RangeExceededException if the seed is out of rane (0-36 inclusive)
     */
    public XoxoMessage encrypt(String message, int seed) throws RangeExceededException {
        if (seed<0 || seed>36) throw new RangeExceededException("The seed is out of range (0<=seed<=36)");
        String encryptedMessage = this.encryptMessage(message);
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey, seed));
    }

    /**
     * Runs the encryption algorithm to turn the message string
     * into an ecrypted message string.
     *
     * @param message The message that wants to be encrypted.
     * @return The encrypted message string.
     * @throws SizeTooBigException       if the message is more than 10Kbit
     * @throws InvalidCharacterException if the key characters is not A-Z, a-z or @
     */
    private String encryptMessage(String message) throws SizeTooBigException{
        final int length = message.length();
        String encryptedMessage = "";
        if (message.getBytes().length*8 >= 10000) throw new SizeTooBigException("Message size is more than 10Kbit");
        for (int i = 0; i < length; i++) {
            int m = message.charAt(i);
            int k = this.kissKey.keyAt(i) - 'a';
            int value = m ^ k;
            encryptedMessage += (char) value;
        }
        return encryptedMessage;
    }
}

