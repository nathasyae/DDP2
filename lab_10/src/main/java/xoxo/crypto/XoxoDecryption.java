package xoxo.crypto;

import xoxo.exceptions.SizeTooBigException;

import static com.sun.org.apache.bcel.internal.Constants.MAX_BYTE;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) throws SizeTooBigException {
        final int length = encryptedMessage.length();
        String decryptedMessage = "";
        if (encryptedMessage.getBytes().length*8 >= 10000) throw new SizeTooBigException("Message size is more than 10Kbit");
        for (int i = 0; i < length; i++) {
            int m = encryptedMessage.charAt(i);
            int a = i % this.hugKeyString.length() ^ seed;
            int b = a - 'a';
            int value = i ^ b;
            decryptedMessage += (char) value;
        }
        return decryptedMessage;
    }




}
