package xoxo;

import org.omg.CORBA.DynAnyPackage.Invalid;
import xoxo.XoxoView;
import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.RangeExceededException;

import xoxo.util.XoxoMessage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static xoxo.key.HugKey.DEFAULT_SEED;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Nathasya Eliora K.
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    XoxoMessage encryptedMsg = null;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        this.gui.setDecryptFunction(e->doDecrypt());
        this.gui.setEncryptFunction(e->doEncrypt());
    }

    /**
     * Mari mendecrypt disini
     */
    public void doDecrypt(){
        gui.clearLog();
        //memasukkan info2 yg dibutuhkan
        String hugKey = this.gui.getKeyText();
        String message = this.gui.getMessageText();
        String seedStr = this.gui.getSeedText();
        int seed;
        if (seedStr.equals("")){
            seed = DEFAULT_SEED;
        } else {
            seed = Integer.parseInt(seedStr);
        }
        //doo
        XoxoDecryption decryptOb = new XoxoDecryption(hugKey);
        String decryptedMsg = decryptOb.decrypt(message,seed);

        decryptedWriter(decryptedMsg);
    }

    /**
     * Mari menenkrypt disini
     */
    public void doEncrypt(){
        gui.clearLog();
        //memasukkan info2 yg dibutuhkan
        String kissKey = this.gui.getKeyText();
        String message = this.gui.getMessageText();
        XoxoEncryption encryptOb = null;
        String seedStr = gui.getSeedText();
        int seed;
        if (seedStr.equals("")){
            seed = DEFAULT_SEED;
        } else {
            seed = Integer.parseInt(seedStr);
        }

        //doo
        try {
            encryptOb = new XoxoEncryption(kissKey);
        } catch (KeyTooLongException e) {
            gui.setWarning("Key length must not exceed 28");
        } catch (InvalidCharacterException e) {
            gui.setWarning("Key can contain only A-Z, a-z, and @");
        }

        try {
            encryptedMsg = encryptOb.encrypt(message,seed);
        } catch (RangeExceededException e) {
            gui.setWarning("Seed must between 0 and 36 (inclusive)");
        } catch (NumberFormatException e) {
            gui.setWarning("Seed must be a number!");
        } catch (xoxo.exceptions.SizeTooBigException e) {
            gui.setWarning("Message size too big (more than 10Kbit)!");
        } catch (NullPointerException e) {
            gui.setWarning("Null pointer! Please check your program again.");
        }

        encryptedWriter(encryptedMsg);

    }

    /**
     * to process output of decrypt action
     */

    public void decryptedWriter(String decryptMsg) {
        File file = new File(decryptMsg+".txt");
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file, true);
            writer.write(decryptMsg);
            writer.flush();
            writer.close();
            gui.appendLog("Decrypted text: " + decryptMsg);
        } catch (IOException e) {
            gui.appendLog("Decrypt failed! Cannot write file.");
        }
    }

    /**
     * to process output of encrypt action
     */

    public void encryptedWriter(XoxoMessage encryptMsg) {
        File file = new File(encryptMsg.getEncryptedMessage()+".enc");
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file, true);
            writer.write(encryptMsg.getEncryptedMessage() + " " + encryptMsg.getHugKey().getKeyString());
            writer.flush();
            writer.close();
            gui.appendLog("Encrypted text: " + encryptMsg.getEncryptedMessage());
            gui.appendLog("Hug key: " + encryptMsg.getHugKey().getKeyString());
        } catch (IOException e) {
            gui.appendLog("Encrypt failed! Cannot create file.");
        }

    }


}
