package xoxo.exceptions;

/**
 * Ketika angka x<0 atau x>36
 */
public class RangeExceededException  extends RuntimeException{
    /**
     * Class constructor.
     */
    public RangeExceededException(String message){
        super(message);
    }

}