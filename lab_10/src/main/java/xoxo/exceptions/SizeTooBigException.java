package xoxo.exceptions;

/**
 * Ketika data lebih besar dari maksimal data yang diinginkan (10Kbit)
 */
public class SizeTooBigException extends RuntimeException{
    /**
     * Class constructor.
     */
    public SizeTooBigException(String message){
        super(message);
    }
    
}