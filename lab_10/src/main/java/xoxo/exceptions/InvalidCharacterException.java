package xoxo.exceptions;

/**
 * Exception ketika mengandung di luar huruf A-Z, a-z, dan @
 */
public class InvalidCharacterException extends RuntimeException{
    /**
     * Class constructor.
     */
    public InvalidCharacterException(String message){
        super(message);
    }

}