/**
<<<<<<< HEAD:lab_5/src/main/java/BingoCard.java
 * Nathasya Eliora K.
 * 1706979404
 * DDP 2 Kelas B
 * LAB 5
=======
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
>>>>>>> f78c0e7742338c8c37b272c05a7ad43888206b02:lab_instructions/lab_5/BingoCard.java
 */

import java.util.Arrays;

public class BingoCard {
	private Number[][] numbers;
	private boolean isBingo = false;

	public BingoCard(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num) {
		//TODO Implement
		boolean flag = false;
		for (int baris = 0; baris < 5; baris++) {
			for (int kolom = 0; kolom < 5; kolom++) {
				if (numbers[baris][kolom].getValue() == num) {
					flag = true;

					if (this.numbers[baris][kolom].isChecked() == false) {
						this.numbers[baris][kolom].setChecked(true);
						this.checkTheBingo();
						//for(int ii =0;ii<5;ii++){
						//	for(int jj=0;jj<5;jj++){
						//		System.out.print(numbers[ii][jj].isChecked());
						//	}
						//	System.out.println();
						//}
						return (num + " tersilang");
					} else {
						return (num + " sebelumnya sudah tersilang");
					}
				}// else {
				//continue;
				//}
			}
		}
			if (!flag) {
				return ("Kartu tidak memiliki angka " + num);
			}return "";
	}


	public void checkTheBingo() {
		boolean cek = true;
		int counter=0;

		//horizontal
		for (int baris = 0; baris < 5; baris++) {
			cek = true;
			for(int kolom = 0;kolom < 5;kolom ++){
				if(this.numbers[baris][kolom].isChecked() == false){
					cek = false;
					break;
				}else{
					counter+=1; }
			} if (cek==false){break;}

		}
		if (cek){this.setBingo(cek);return;}
		cek = true;
		//vertical
		for (int k = 0; k < 5; k++) {
			if (numbers[0][k].isChecked() && numbers[1][k].isChecked() && numbers[2][k].isChecked() && numbers[3][k].isChecked() && numbers[4][k].isChecked()){
				this.setBingo(true);
			}
		}
		//diagonal kiri
		if (numbers[0][0].isChecked() && numbers[1][1].isChecked() && numbers[2][2].isChecked() &&
				numbers[3][3].isChecked() && numbers[4][4].isChecked()) {
			this.setBingo(true);
		}
		//diagonal kanan
		else if (numbers[4][0].isChecked() && numbers[3][1].isChecked() && numbers[2][2].isChecked() &&
				numbers[1][3].isChecked() && numbers[0][4].isChecked()) {
			this.setBingo(true);
		}
		else {
			this.setBingo(false);
		}
	}

	public String info () {
			//TODO Implement
			String information = "";
			for (int baris = 0; baris < 5; baris++) {
				information += "|";
				for (int kolom = 0; kolom < 5; kolom++) {
					if (numbers[baris][kolom].isChecked()) {
						information += " X  |";
						}
					 else {
						if (String.valueOf(numbers[baris][kolom].getValue()).length() == 1) {
							information += " 0" + numbers[baris][kolom].getValue() + " |";
						} else {
							information += " "+numbers[baris][kolom].getValue() + " |";
						}
					}
				}
				if (baris!=4){
					information += "\n";
				}
			}
			return information;
		}

			public String restart () {
				//TODO Implement
				for (int baris = 0; baris < 5; baris++) {
					for (int kolom = 0; kolom < 5; kolom++) {
						numbers[baris][kolom].setChecked(false);
					}
				}
				return ("Mulligan!");
			}
		}