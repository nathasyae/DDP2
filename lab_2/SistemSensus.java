import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.

 * Code Author (Mahasiswa):
 * @author Nathasya Eliora Kristianti, NPM 1706979404, Kelas DDP D, GitLab Account: nathasyae
 */

public class SistemSensus {
    public static void main(String[] args) {
        // Buat input scanner baru
        Scanner input = new Scanner(System.in);
        String warning = "WARNING: Keluarga ini tidak perlu direlokasi";

        // TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
        // User Interface untuk meminta masukan
        System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
                "--------------------\n" +
                "Nama Kepala Keluarga   : ");
        String nama = input.nextLine();
        System.out.print("Alamat Rumah           : ");
        String alamat = input.nextLine();
        System.out.print("Panjang Tubuh (cm)     : ");
        int panjang = Integer.parseInt(input.nextLine());
        if (!(0 < panjang && panjang <= 250)) {
            System.out.println(warning);
            return;
        }

        System.out.print("Lebar Tubuh (cm)       : ");
        int lebar = Integer.parseInt(input.nextLine());
        if (!(0 < lebar && lebar <= 250)) {
            System.out.println(warning);
            return;
        }

        System.out.print("Tinggi Tubuh (cm)      : ");
        int tinggi = Integer.parseInt(input.nextLine());
        if (!(0 < tinggi && tinggi <= 250)) {
            System.out.println(warning);
            return;
        }

        System.out.print("Berat Tubuh (kg)       : ");
        float berat = Float.parseFloat(input.nextLine());
        if (!(0 < berat && berat <= 150)) {
            System.out.println(warning);
            return;
        }

        System.out.print("Jumlah Anggota Keluarga: ");
        int makanan = Integer.parseInt(input.nextLine());
        if (!(0 < makanan && makanan <= 20)) {
            System.out.println(warning);
            return;
        }

        System.out.print("Tanggal Lahir          : ");
        String tanggalLahir = input.nextLine();
        System.out.print("Catatan Tambahan       : ");
        String catatan = input.nextLine();
        System.out.print("Jumlah Cetakan Data    : ");
        int jumlahCetakan = Integer.parseInt(input.nextLine());

        // TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
        // TODO Hitung rasio berat per volume (rumus lihat soal)
        int rasio = (int) (berat * 1000000) / (panjang * lebar * tinggi);

        for (int i = 0; i < jumlahCetakan; i++) {
            // TODO Minta masukan terkait nama penerima hasil cetak data
            System.out.println();
            System.out.print("Pencetakan " + (i + 1) + " dari " + jumlahCetakan + " untuk: ");
            String penerima = (input.nextLine()).toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

            // TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
            String hasil = "DATA SIAP DICETAK UNTUK " + penerima + "\n" +
                    "--------------------";
            System.out.println(hasil);
            System.out.println(nama + " - " + alamat);
            System.out.println("Lahir pada tanggal " + tanggalLahir);
            System.out.println("Rasio Berat Per Volume = " + rasio + "kg/m^3");

            // TODO Periksa ada catatan atau tidak
            if ((catatan).length() > 0) {
                catatan = "Catatan: " + catatan;
            } else {
                System.out.println("Tidak ada catatan tambahan");
            }
        }
        // TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
        // TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal
        int asciiNama = 0;
        for (int a = 0; a < nama.length(); a++) {
            asciiNama = asciiNama + (int) nama.charAt(a);
        }

        // TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
        String nomorKeluarga = (nama.charAt(0) + String.valueOf(((panjang * tinggi * lebar) + asciiNama) % 10000));

        // TODO Hitung anggaran makanan per tahun (rumus lihat soal)
        int anggaran = (50000) * (365) * makanan;

        // TODO Hitung umur dari tanggalLahir (rumus lihat soal)

        String[] tanggal = tanggalLahir.split("-"); // lihat hint jika bingung
        int tahunLahir = Integer.parseInt(tanggal[2]);
        int umur = (2018) - tahunLahir;

        // TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
        String tempat = "";
        String kabupaten = "";

        if (umur >= 0 && umur <= 18) {
            kabupaten = "Rotunda";
            tempat = "PPMT";
        } else if (umur >= 19 && umur <= 1018) {
            if (anggaran > 0 && anggaran <= 100000000) {
                kabupaten = "Sastra";
                tempat = "Teksas";
            } else if (anggaran > 100000000) {
                kabupaten = "Margonda";
                tempat = "Mares";
            }
        }
        // TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
        String rekomendasi = "\nREKOMENDASI APARTEMEN\n" +
                "--------------------";
        System.out.println(rekomendasi);
        System.out.println("MENGETAHUI: Identitas Keluarga: " + nama + " " + nomorKeluarga);
        System.out.println("MENIMBANG: Anggaran makanan tahunan: Rp " + anggaran+"\n"+
        "            Umur kepala keluarga: "+umur+" tahun");
        System.out.println("MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di: " + tempat + ", kabupaten " + kabupaten);

        input.close();

    }

}





