/**
 * Nathasya Eliora K.
 * 1706979404
 * DDP 2 Kelas B
 * Lab 6
 */

package ticket;

import movie.Movie;
import theater.Theater;

public class Ticket {
    private Movie film;
    private String jadwal;
    private boolean is3D;
    private int harga;

    //setter getter
    public Movie getFilm() {
        return film;
    }

    public void setFilm(Movie film) {
        this.film = film;
    }

    public String getJadwal() {
        return jadwal;
    }

    public void setJadwal(String jadwal) {
        this.jadwal = jadwal;
    }

    public boolean isIs3D() {
        return is3D;
    }

    public void setIs3D(boolean is3D) {
        this.is3D = is3D;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public int getHarga() {
        return this.harga;
    }

    //constructor
    public Ticket(Movie film, String jadwal, boolean is3D){
        this.film = film;
        this.jadwal = jadwal;
        this.is3D = is3D;
        this.harga = harga;
    }

    //menghitung harga tiket
    public int hitungHarga(String jadwal, String jenisDimensi){
        this.harga = 60000;
        if (jadwal.equals("Sabtu")||jadwal.equals("Minggu")){
            this.harga+=40000;
        }

        if (jenisDimensi.equals("3 Dimensi")){
            //Ticket.setIs3D(true);
            this.harga+=0.2*this.harga;
        }
        return this.harga;
    }

    //ngeprint informasi
    public void printInfo(){
        //System.out.println("Total uang yang dimiliki Koh Mas : " +saldo);
        String jenisDimensi = "Biasa";
        if (this.is3D==true){
            jenisDimensi = "3 Dimensi";
        }
        System.out.println("------------------------------------------------------------------");
        System.out.println("Film: "+film.getJudul()+"\n"+
                "Jadwal Tayang: "+this.jadwal+
                "Jenis: "+jenisDimensi);
        System.out.println("------------------------------------------------------------------");
    }

}
