/**
 * Nathasya Eliora K.
 * 1706979404
 * DDP 2 Kelas B
 * Lab 6
 */

package customer;

import movie.Movie;
import theater.Theater;
import ticket.Ticket;
import java.util.ArrayList;

public class Customer {
    private String nama;
    private int umur;
    private boolean jenisKelamin; //true jika P, false jika L
    private Movie theMovie;
    private Movie[] filmTersedia;
    private ArrayList<Ticket> tiketTersedia;
    private Ticket ticketnya;
    private int price;
    private boolean ada;

    //setter getter
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public boolean isJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(boolean jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    //constructor
    public Customer(String nama, boolean jenisKelamin, int umur) {
        this.nama = nama;
        this.umur = umur;
        this.jenisKelamin = jenisKelamin;
    }

    //method untuk mengorder tiket
    public Ticket orderTicket(Theater bioskop, String judul, String jadwal, String jenisDimensi) {
        filmTersedia = bioskop.getDaftarFilm();
        tiketTersedia = bioskop.getkumpulanTiket();
        ada = false;
        boolean is3D = false;
        if (jenisDimensi.equals("3 Dimensi")) {
            is3D = true;
            //System.out.println("is3dnya tru");
        }
        //menyimpan film yang mau ditonton ke variabel
        for (int i = 0; i < filmTersedia.length; i++) {
            if (filmTersedia[i].getJudul().equals(judul)) {
                theMovie = filmTersedia[i];
                //System.out.println("themovienya "+theMovie.getJudul());
            }
        }

        //membuat object Ticket berdasar film jadwal dan jenis dimensi
        ticketnya = new Ticket(theMovie, jadwal, is3D);

        int hasil = 0;
        for (int i = 0; i < tiketTersedia.size(); i++) {
            //cek ada atau tidak filmnya
            if ((tiketTersedia.get(i).getFilm().equals(ticketnya.getFilm()))&&
                    (tiketTersedia.get(i).getJadwal().equals(ticketnya.getJadwal())) &&
                            (tiketTersedia.get(i).isIs3D()==ticketnya.isIs3D())){
                ada = true;
                hasil = i;
                //System.out.println("adanya tru");
            }
        }
        if (ada == false) {
            //System.out.println("adanya fals");
            //System.out.printf("Tiket untuk film %s jenis %s dengan jadwal %s tidak tersedia di %s", judul,jenisDimensi,jadwal,bioskop.getBioskop());
            //System.out.println();
            System.out.println("Tiket untuk film " + judul + " jenis " + jenisDimensi + " dengan jadwal " +
                  jadwal + " tidak tersedia di " + bioskop.getBioskop());
        } else {
            price = ticketnya.hitungHarga(jadwal, jenisDimensi);
            //cek umur
            if (theMovie.getGenre().equals("Remaja")) {
                if (this.umur < 13) {
                    System.out.println(this.nama + " masih belum cukup umur untuk menonton " +
                            theMovie.getJudul() + " dengan rating " + theMovie.getRating());
                } else {
                    beli(bioskop, ticketnya);
                }
            } else if (theMovie.getGenre().equals("Dewasa")) {
                if (this.umur < 17) {
                    System.out.println(this.nama + " masih belum cukup umur untuk menonton " +
                            theMovie.getJudul() + " dengan rating " + theMovie.getGenre());
                } else {
                    beli(bioskop, ticketnya);
                }
            } else {
                beli(bioskop, ticketnya);
            }
        }
        return tiketTersedia.get(hasil);
    }

    //method untuk membeli tiket
    public void beli(Theater bioskop, Ticket ticket) {
        String jenisDimensi = "3 Dimensi";
        if (ticketnya.isIs3D() == false) {
            jenisDimensi = "Biasa";
        }
        if (ada) {
            bioskop.setSaldo(bioskop.getSaldo() + price);
            bioskop.getkumpulanTiket().remove(ticketnya);
            System.out.println(this.nama + " telah membeli tiket " + theMovie.getJudul() + " jenis " + jenisDimensi + " di " + bioskop.getBioskop() + " pada hari "+ ticket.getJadwal() + " seharga Rp. " + price);
        }
    }

    //method untuk mencari movie
    public void findMovie(Theater bioskop, String judul){
        boolean found = false;
        filmTersedia = bioskop.getDaftarFilm();
        for (int i = 0; i<filmTersedia.length;i++){
            if (filmTersedia[i].getJudul().equals(judul)){
                found = true;
                theMovie = filmTersedia[i];
            }
        }
        if (found){
            theMovie.printInfo();
        } else {
            System.out.println("Film " + judul + " yang dicari " + this.nama + " tidak ada di bioskop " + bioskop.getBioskop());
        }
    }
}

   /** public void findMovie(Theater bioskop, String judul) {
        boolean found = false;
        filmTersedia = bioskop.getDaftarFilm();
        tiketTersedia = bioskop.getkumpulanTiket();
        for (int i = 0; i < filmTersedia.length; i++) {
            if (filmTersedia[i].getJudul().equals(judul)) {
                theMovie = filmTersedia[i];
                ada = true;
            }
        }
        if (theMovie==null){
            System.out.println("Film " + theMovie.getJudul() + " yang dicari " + this.nama + " tidak ada di bioskop " + bioskop.getBioskop());
        } else {
            theMovie.printInfo();
        }
    }
}
*/






















    //methods
/**
    public Ticket orderTicket(Theater bioskop, String judul, String jadwal, String jenisDimensi) {
        filmTersedia = bioskop.getDaftarFilm();
        tiketTersedia = bioskop.getkumpulanTiket();
        boolean is3D = false;
        if (jenisDimensi.equals("3 Dimensi")) {
            is3D = true;
        }

 //cek ada ga filmnya
 tiketTersedia = bioskop.getkumpulanTiket();
 for (int i = 0; i<tiketTersedia.size();i++){
 if (tiketTersedia.get(i).getFilm().equals(judul)){
 if (tiketTersedia.get(i).getJadwal().equals(jadwal)){
 if (tiketTersedia.get(i).isIs3D()==is3D){
 ada = true;
 }
 }
 }
 }

        //cek ada ga
        ticketnya = new Ticket(theMovie, jadwal, is3D);
        if (adaGaFilmnya(bioskop, judul, jadwal, is3D) == false) {
            System.out.println("Tiket untuk film" + judul + " jenis " + jenisDimensi + " pada hari " +
                    jadwal + " tidak tersedia di " + bioskop);
        }
        //cek umur
        else {
            ticketnya = new Ticket(theMovie, jadwal, is3D);
            for (int i = 0; i < tiketTersedia.size(); i++) {
                if (tiketTersedia.get(i).getFilm().getJudul().equals(judul)) {
                    ticketnya = tiketTersedia.get(i);
                }
            }
            int price = ticketnya.hitungHarga(jadwal, jenisDimensi);
            if (theMovie.getGenre().equals("Remaja")) {
                if (this.umur < 13) {
                    System.out.println(this.nama + " masih belum cukup umur untuk menonton " +
                            theMovie.getJudul() + " dengan rating " + theMovie.getRating());
                } else {
                    beli(bioskop, judul, jenisDimensi, price);
                }
            } else if (theMovie.getGenre().equals("Remaja")) {
                if (this.umur < 17) {
                    System.out.println(this.nama + " masih belum cukup umur untuk menonton " +
                            theMovie.getJudul() + " dengan rating " + theMovie.getRating());
                } else {
                    beli(bioskop, judul, jenisDimensi, price);
                }
            } else {
                beli(bioskop, judul, jenisDimensi, price);
            }
        }
        return ticketnya;
    }

    public void beli(Theater bioskop, String judul, String jenisDimensi, int price){
        bioskop.setSaldo(bioskop.getSaldo() + price);
        for (int i = 0; i<bioskop.getkumpulanTiket().size(); i++){
            if (bioskop.getkumpulanTiket().get(i).equals(judul)){
                bioskop.getkumpulanTiket().remove(bioskop.getkumpulanTiket().get(i));
            }
        System.out.println(this.nama + " telah membeli tiket " + judul + " jenis " + jenisDimensi + " seharga Rp. " + price);
            }
        }

    public boolean adaGaFilmnya(Theater bioskop, String judul){
        boolean ada = false;
        //filmTersedia = bioskop.getDaftarFilm();
        for (int i = 0; i<filmTersedia.length; i++){
            //System.out.println();
            if (filmTersedia[i].getJudul().equals(judul)){
                ada = true;
                theMovie = filmTersedia[i];
            }
        }
        return ada;
    }

    public boolean adaGaFilmnya(Theater bioskop, String judul,String jadwal, boolean is3D){
        boolean ada = false;
        for (int i = 0; i<filmTersedia.length; i++) {
            for (int j = 0; j<tiketTersedia.size();j++) {
                for (int k = 0;k<tiketTersedia.size();k++) {
                    //System.out.println();
                    if (filmTersedia[i].getJudul().equals(judul)) {
                        if (tiketTersedia.get(j).getJadwal().equals(jadwal)) {
                            if (tiketTersedia.get(k).isIs3D()==is3D) {
                                ada = true;
                                theMovie = filmTersedia[i];
                            }
                        }
                    }
                }
            }
        }
        return ada;
    }

    public void findMovie(Theater bioskop, String judul){
        if (adaGaFilmnya(bioskop,judul)) {
            theMovie.printInfo();
        }else{
            System.out.println("Film "+theMovie.getJudul()+" yang dicari "+this.nama+" tidak ada di bioskop "+bioskop.getBioskop());
        }
    }
}

*/
//ngeprint22
/**public static Ticket orderTicket(Theater bioskop, String judul, String jadwal, String jenisDimensi) {
 Movie filmnya = new Movie(String judul, String genre, int durasi, String rating, String jenisOrigin);
 if (jenisDimensi.equals("Biasa")){
 filmnya.is3D = true;
 }else{
 is3D=false;
 }
 Ticket ticketnya = new Ticket(Movie film, String jadwal, boolean is3D);
 int price = Ticket.hitungHarga(jadwal, jenisDimensi);

 if (Movie.getRating().equals("Remaja")) {
 if (this.umur < 13) {
 System.out.println(this.nama + " masih belum cukup umur untuk menonton " + judul + " dengan rating " + Movie.getRating());
 } else if (Movie.getRating().equals("Dewasa")) {
 if (this.umur < 17) {
 System.out.println(this.nama + " masih belum cukup umur untuk menonton " + judul + " dengan rating " + Movie.getRating());
 }
 } else {
 System.out.println(this.nama + " telah membeli tiket " + judul + " jenis " + jenisDimensi +
 "di " + bioskop + " pada hari " + jadwal + " seharga Rp. " + price);
 bioskop.setSaldo(bioskop.getSaldo() + price);
 bioskop.setDaftarFilm(bioskop.getSaldo() - 1);
 //berhasil beli tiket
 System.out.println(this.nama + " telah membeli tiket " + judul + " jenis " + jenisDimensi + " seharga Rp. " + price);
 }
 return ticketnya;
 }
 }
 */



