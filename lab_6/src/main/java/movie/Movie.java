/**
 * Nathasya Eliora K.
 * 1706979404
 * DDP 2 Kelas B
 * Lab 6
 */

package movie;

public class Movie {
    private String judul;
    private String genre;
    private int durasi;
    private String rating;
    private String jenisOrigin;


    //setter getter
    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDurasi() {
        return durasi;
    }

    public void setDurasi(int durasi) {
        this.durasi = durasi;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getJenisOrigin() {
        return jenisOrigin;
    }

    public void setJenisOrigin(String jenisOrigin) {
        this.jenisOrigin = jenisOrigin;
    }

    //constructor
    public Movie(String judul, String genre, int durasi, String rating, String jenisOrigin){
        this.judul = judul;
        this.genre = genre;
        this.durasi = durasi;
        this.rating = rating;
        this.jenisOrigin = jenisOrigin;
    }

    //method untuk print informasi
    public void printInfo(){
        System.out.println("------------------------------------------------------------------");
        System.out.println("Judul\t: "+this.judul+"\n"+
                "Genre\t: "+this.rating+"\n"+
                "Durasi\t: "+this.durasi+"\n"+
                "Rating\t: "+this.genre+"\n"+
                "Jenis\t: Film "+this.jenisOrigin);
        System.out.println("------------------------------------------------------------------");
    }
}
