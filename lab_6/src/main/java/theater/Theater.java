/**
 * Nathasya Eliora K.
 * 1706979404
 * DDP 2 Kelas B
 * Lab 6
 */

package theater;

import movie.Movie;
import ticket.Ticket;

import java.util.ArrayList;

public class Theater {
    private String bioskop;
    private int saldo;
    private ArrayList<Ticket> kumpulanTiket;
    private Movie[] daftarFilm;

    //setter getter
    public String getBioskop() {
        return bioskop;
    }

    public void setBioskop(String bioskop) {
        this.bioskop = bioskop;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public ArrayList<Ticket> getkumpulanTiket() {
        return kumpulanTiket;
    }

    public void setkumpulanTiket(ArrayList<Ticket> kumpulanTiket) {
        this.kumpulanTiket = kumpulanTiket;
    }

    public Movie[] getDaftarFilm() {
        return daftarFilm;
    }

    public void setDaftarFilm(Movie[] daftarFilm) {
        this.daftarFilm = daftarFilm;
    }

    //constructor
    public Theater(String bioskop, int saldo, ArrayList <Ticket> kumpulanTiket, Movie[] daftarFilm) {
        this.bioskop = bioskop;
        this.saldo = saldo;
        this.kumpulanTiket = kumpulanTiket;
        this.daftarFilm = daftarFilm;
    }

    //method untuk print informasi
    public void printInfo() {
        System.out.println("------------------------------------------------------------------");
        System.out.println("Bioskop\t\t\t\t\t: " + this.bioskop + "\n" +
                "Saldo Kas\t\t\t\t: " + this.saldo + "\n" +
                "Jumlah tiket tersedia\t: " + kumpulanTiket.size());
        System.out.print("Daftar Film tersedia\t: ");
        for (int i = 0; i < this.daftarFilm.length; i++) {
            if (i == this.daftarFilm.length - 1) {
                System.out.print(this.daftarFilm[i].getJudul() + "\n");
            } else {
                System.out.print(this.daftarFilm[i].getJudul() + ", ");
            }
        }
        System.out.println("------------------------------------------------------------------");
    }

    public static void printTotalRevenueEarned(Theater[] theaters){
        int totalSaldo = 0;
        for (int i = 0; i<theaters.length; i++){
            totalSaldo+=theaters[i].getSaldo();
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp." + " " + totalSaldo);
        System.out.println("------------------------------------------------------------------");
        for (int i = 0; i<theaters.length; i++){
            System.out.println("Bioskop\t\t: "+ theaters[i].bioskop);
            System.out.println("Saldo Kas\t: Rp. "+theaters[i].saldo+"\n");
        }
        System.out.println("------------------------------------------------------------------");
    }

}
