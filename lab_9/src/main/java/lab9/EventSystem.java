package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.ArrayList;
import java.util.Date;
import java.text.ParseException;

/**
* Class representing event managing system
*/
public class EventSystem {
    /**
     * List of events
     */
    public static ArrayList<Event> events;

    /**
     * List of users
     */
    public static ArrayList<User> users;

    /**
     * Constructor. Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    //find object event
    public Event getEvent(String name) {
        for (Event i : events) {
            if (i.getName().equals(name)) {
                return i;
            }
        }
        return null;
    }

    //cek datenya valid ga
    public static boolean checkDate(Date date1, Date date2) {
        if (date1.compareTo(date2) < 0) {
            return true;
        } else {
            return false;
        }
    }

    //menambahkan event
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) throws ParseException {
        //cek ada ga
        if (getEvent(name) == null) {
            //cek waktu mulai > waktu selesai ga
            Date date1 = Event.converttoDate(startTimeStr);
            Date date2 = Event.converttoDate(endTimeStr);

            if (checkDate(date1, date2) == true) {
                events.add(new Event(name, date1, date2, costPerHourStr));
                return "Event " + name + " berhasil ditambahkan!";
            }
            return "Waktu yang diinputkan tidak valid!";

        }
        return "Event " + name + " sudah ada!";
    }

    //find obj user
    public User getUser(String name) {
        for (User i : users) {
            if (i.getName().equals(name)) {
                return i;
            }
        }
        return null;
    }

    //add user
    public String addUser(String name) {
        if (getUser(name) == null) {
            users.add(new User(name));
            return "User " + name + " berhasil ditambahkan!";
        } else {
            return "User " + name + " sudah ada!";
        }
    }

    //mendaftarkan diri ke event
    public String registerToEvent(String userName, String eventName) {
        //cek ada ga
        if (getUser(userName) != null && getEvent(eventName) != null) {
            //kalo ada, add
            if (getUser(userName).addEvent(getEvent(eventName)) == true) {
                return userName + " berencana menghadiri " + eventName + "!";
            } else {
                return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
            }
        } else if (getUser(userName) == null && getEvent(eventName) == null) {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        } else if (getUser(userName) == null) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        } else if (getEvent(eventName) == null) {
            return "Tidak ada acara dengan nama " + eventName;
        } return "";
    }
}
