package lab9.event;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>
{
    /** Name of event */
    private String name;

    /** Beginning and end time of event */
    private Date startTime;

    private Date endTime;

    /** Cost of event */
    private BigInteger cost;

    /**
     * Getter
     * methods to get private information of the class
     */
    public Date getStartTime(){
        return this.startTime;
    }

    public Date getEndTime(){
        return this.endTime;
    }

    public String getStartTimeStr() {
        //21-05-2016, 11:02:34
        String startfix = new SimpleDateFormat("dd-MM-yyy, HH:mm:ss").format(this.startTime);
        return startfix;
    }

    public String getEndTimeStr() {
        String endfix = new SimpleDateFormat("dd-MM-yyy, HH:mm:ss").format(this.endTime);
        return endfix;
    }

    public BigInteger getCost() {
        return this.cost;
    }

    /**
     * Constructor
     * Initializes a event object with given name, startTime, endTime, and cost
     */
    public Event(String name, Date startTime, Date endTime, String costPerHourStr){
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.cost = new BigInteger(costPerHourStr);
    }

    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName()
    {
        return this.name;
    }

    public static Date converttoDate(String time) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        Date dDate = sdf.parse(time);
        return dDate;
    }

    public String toString(){
        return (this.name+
                "\nWaktu mulai: "+getStartTimeStr()+
                "\nWaktu selesai: "+getEndTimeStr()+
                "\nBiaya kehadiran: "+getCost());
    }


    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.

    public boolean overlapsWith(Event other) {
        //event2 duluan, baru event 1
        if (this.startTime.after(other.endTime)) {
            return false;
        } //event1 duluan, baru event 2
        else if (this.endTime.before(other.startTime)) {
            return false;
        } //kl ngepas
        else if (this.endTime.compareTo(other.startTime) == 0 || other.endTime.compareTo(this.startTime) == 0) {
            return false;
        } else {
            return true;
        }
    }


    @Override
    public int compareTo(Event other) {
        return this.getStartTime().compareTo(other.getEndTime());
    }
        /*
        if (this.startTime.compareTo(other.endTime)<=0 || other.startTime.compareTo(this.endTime)<=0 ){
            return false;
        } else {
            return true;
        }
        */

        /**if (this.startTime.compareTo(other.startTime) <= 0 && this.endTime.compareTo(other.startTime) >= 0){
            return false;
        } else if (this.startTime.compareTo(other.endTime) <= 0 && this.endTime.compareTo(other.endTime) >= 0){
            return false;
        } else if (this.startTime.compareTo(other.endTime) <= 0 && this.endTime.compareTo(other.startTime) >= 0){
            return false;
        } else {
            return true;
        }

         public boolean overlapsWith(Event other){

         return(this.startTime.compareTo(other.startTime) <= 0 && this.endTime.compareTo(other.startTime) >= 0)
         || (this.startTime.compareTo(other.endTime) <= 0 && this.endTime.compareTo(other.endTime) >= 0)
         || (this.startTime.compareTo(other.endTime) <= 0 && this.endTime.compareTo(other.startTime) >= 0);
         }
         */

}
